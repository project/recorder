********************************************************************
                     D R U P A L    M O D U L E                         
********************************************************************
Name: Recorder module 
Version: 1.1
Author: Robert Laarhoven 
Email: robert at polder.net 
Last update: 25 March 2003 
Drupal: 4.x

********************************************************************
DESCRIPTION:

With this Drupal module you can record (detect, track and log) 
specific visitors, hosts, and user agents.

The recorder can be used for several purposes, as debugging and 
solving user problems, monitoring security, tracking trolls, 
crawlers, and search engine spiders, page and site statistics 
(samples), and simple usability research.

When the recorder module is enabled, normal users will not notice 
much preformance difference (no extra SQL lookups, uses variables 
table), targets will have 3 or 4 extra SQL requests.

********************************************************************
INSTALLATION: 

Copy the recorder.module file into your Drupal modules/ directory, 
and insert recorder.sql into your Drupal database. Enable the module 
in your administration settings. 

If you want to enable the browser detection, you need to install 
and enable the agents module too. Sessioning all users is also 
recommended (see agents module README.txt).

********************************************************************
TODO:
- log all HTTP headers.
- playback function
- bug with session counter when deleting session?
- backup function(?)
- using new $_SESSION, $_GET etc. arrays
- cron function
- remote control

********************************************************************

