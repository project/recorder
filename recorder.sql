CREATE TABLE rec_sess (
   sid int(10) DEFAULT '0' NOT NULL auto_increment,
   rid tinyint(3) DEFAULT '0' NOT NULL,
   timestamp int(11) DEFAULT '0' NOT NULL,
   host varchar(128) NOT NULL,
   domain varchar(255) NOT NULL,
   proxy varchar(128) NOT NULL,
   agent varchar(255) NOT NULL,
   username varchar(32) NOT NULL,
   note varchar(255) NOT NULL,
   PRIMARY KEY (sid)
);

CREATE TABLE rec_log (
   lid int(10) DEFAULT '0' NOT NULL auto_increment,
   rid int(2) DEFAULT '0' NOT NULL,
   sid int(2) DEFAULT '0' NOT NULL,
   phpsid varchar(32) NOT NULL,
   timestamp int(11) DEFAULT '0' NOT NULL,
   referer varchar(255) NOT NULL,
   request varchar(255) NOT NULL,
   get varchar(255) NOT NULL,
   post text,
   cookie varchar(255) NOT NULL,
   session text NOT NULL,
   flags text NOT NULL,
   PRIMARY KEY (lid),
   KEY sid (sid)
);
